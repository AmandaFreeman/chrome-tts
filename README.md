Chrome extension to read selected text. Has a context menu with various options

Options include increasing speech rate for those times when you need to start slow and get faster. 

It's helpful if you struggle to concentrate but you're impatient.

PLus a decrease option for those times when your mind is racing but you need to wind down.