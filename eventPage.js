var RATE = 1.8;

var speak_1_5 = {
    "id": "Speak_1.5",
    "title": "Speak 1.5",
    "contexts": ["selection"]
};
var speak_2_0 = {
    "id": "Speak_2.0",
    "title": "Speak 2.0",
    "contexts": ["selection"]
};
var speak_2_5 = {
    "id": "Speak_2.5",
    "title": "Speak 2.5",
    "contexts": ["selection"]
};
var speak_increase = {
    "id": "Speak_inc",
    "title": "Speak Increase",
    "contexts": ["selection"]
};
var speak_decrease = {
    "id": "Speak_dec",
    "title": "Speak Decrease",
    "contexts": ["selection"]
};
var stop = {
    "id": "Stop",
    "title": "Stop Speaking",
    "contexts": ["selection"]
};

function reverseFibonacci(n) {
    var a = [];
    a[0] = 0 ;
    a[1] = 1 ;
    for (var i = 2; i < n; i++){
        a[i] = a[i - 2] + a[i - 1];
    }
    return a.reverse();

    // not sure where I was going with this but I'm leaving it in in case I can find use of it:
    // for (var i in range(n - 1, -1 , -1)){
    //     yield a[i];
    // }
}

// 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610: fibonacci (not reversed)
function* chunkString(text, direction){
    let splitString = text.split('. ');
    let chunkSize = 2;
    let previousChunksize = 1;
    let chunk = [];

    switch(direction){
        // case 1: rate increasing
        case 1:
            while(chunkSize < splitString.length){
                let chunk = splitString.splice(0, chunkSize);
                let joinedString = chunk.join('. ');
                let newChunkSize = chunkSize + previousChunksize;
                previousChunksize = chunkSize;
                chunkSize = newChunkSize;
                yield joinedString;
            }
            break;

        // case 0: rate decreasing
        case 0:
            let chunksizes = reverseFibonacci(5);
            /* use reverse fibonacci to cover more sentences at fast rate
                and fewer sentences as the rate slows, smoothing the rate of deceleration.
                Ignore the last chunkSize values as they're 1, 1, 2, 3
                then switch to 5-sentence sized chunks until the text runs out
            */
            for (var i=0; i<chunksizes.length - 5; i++){
                let chunk = splitString.splice(0, chunksizes[i]);
                let joinedString = chunk.join('. ');
                yield joinedString;
            }
            for (var j=0; i<splitString.length - 1; j++){
                let chunk = splitString.splice(0, 5);
                let joinedString = chunk.join('. ');
                yield joinedString;
            }
            break;
    }
    
    chunksize = splitString.length;
    chunk = splitString.splice(0, chunkSize);
    let joinedString = chunk.join('. ');
    yield joinedString;
}


function* range(start, end) {
    for (let i = start; i <= end; i++) {
        yield i;
    }
}

// make menu item for 1.5
chrome.contextMenus.create(speak_1_5);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Speak_1.5" && clickData.selectionText){
        chrome.tts.stop();
        chrome.tts.speak(clickData.selectionText, {'rate': 1.5});
    }
});


// make menu item for 2.0
chrome.contextMenus.create(speak_2_0);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Speak_2.0" && clickData.selectionText){
        chrome.tts.stop();
        chrome.tts.speak(clickData.selectionText, {'rate': 2.0});
    }
});

// make menu item for 2.5
chrome.contextMenus.create(speak_2_5);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Speak_2.5" && clickData.selectionText){
        chrome.tts.stop();
        chrome.tts.speak(clickData.selectionText, {'rate': 2.5});
    }
});


// make menu item for start slow and get faster
chrome.contextMenus.create(speak_increase);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Speak_inc" && clickData.selectionText){
        chrome.tts.stop();
        let rate = 1.3;
        for (str of chunkString(clickData.selectionText, 1)){
            chrome.tts.speak(str, {'rate': rate, 'enqueue': true});
            rate = rate >= 2.5 ? 2.5 : rate + 0.1;
        }
    };
});

// make menu item for start fast, and get slower
chrome.contextMenus.create(speak_decrease);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Speak_dec" && clickData.selectionText){
        chrome.tts.stop();
        let rate = 2.5;
        for (str of chunkString(clickData.selectionText, 0)){
            chrome.tts.speak(str, {'rate': rate, 'enqueue': true});
            rate = rate <= 1.2 ? 1.2 : rate - 0.1;
        }
    
        };
});
// stop speaking
chrome.contextMenus.create(stop);
chrome.contextMenus.onClicked.addListener(function(clickData){
    if (clickData.menuItemId == "Stop"){
        chrome.tts.stop();
    }
});
